python-canmatrix (0.9.1~github-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix install paths of utilities. (Closes: #959552)

 -- Adrian Bunk <bunk@debian.org>  Fri, 29 May 2020 18:20:43 +0300

python-canmatrix (0.9.1~github-1) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * New upstream version 0.9.1~github
    * Refresh patches
  * Fix install path of utilities
  * Install examples
  * Declare that building this package does not require "root" powers

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 09 Jan 2020 16:53:49 +0100

python-canmatrix (0.8~github-1) unstable; urgency=medium

  * New upstream version 0.8~github

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Add patch to replace 'pathlib2' on Py3
  * Drop "python-canmatrix" as part of the Python2 removal in bullseye
  * Add more Build-Dependencies (for tests)
  * Disable some broken tests
  * Update d/watch to monitor github rather than pypi
  * Bump dh-compat to 12
  * Bump standards version to 4.4.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 10 Sep 2019 16:07:44 +0200

python-canmatrix (0.6-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Added generic python tests for DebCI
  * Bumped standards version to 4.1.4

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 18 Apr 2018 15:48:52 +0200

python-canmatrix (0.6-2) unstable; urgency=medium

  * Switched canmatrix-utils to Python3
  * Updated d/watch to use pypi.python.org
  * Bumped standards version to 4.1.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 15 Dec 2017 16:20:11 +0100

python-canmatrix (0.6-1) unstable; urgency=medium

  * New upstream version 0.6
  * Generated python*:Recommends from requires.txt
  * Dropped manual Recommends and unused Recommends/Suggests
  * Updated d/copyright
  * Switched Homepage field to https://
  * Bumped standards version to 4.1.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 25 Oct 2017 13:18:13 +0200

python-canmatrix (0.5-1) unstable; urgency=medium

  * New upstream version 0.5
  * Dropped patches included upstream.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 21 Oct 2016 11:05:15 +0200

python-canmatrix (0.4-2) unstable; urgency=medium

  * Factored out canmatrix-utils package
  * Recommend optional libraries for additional formats
  * Python2 support
    * python-canmatrix depends on python-future
  * Fixed Vcs-* stanzas

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 26 Sep 2016 20:57:39 +0200

python-canmatrix (0.4-1) unstable; urgency=medium

  * Initial release (closes: #837806)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 15 Sep 2016 14:43:49 +0200
